﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TimerScript : MonoBehaviour
{
    [Header("General Options")]
    public Text timerText;
    public Text generationText;
    public Text bestTimeText;

    [HideInInspector]
    public int generation;

    // ms, second, minute, hour
    [HideInInspector]
    public float[] time = new float[] { 0, 0, 0, 0 };

    // ms, second, minute, hour
    [HideInInspector]
    public float[] bestTime = new float[] { 0, 0, 0, 0 };

    [HideInInspector]
    public int playSpeed = 1;

    public void resetTimer()
    {
        if (compareBestTime() > 0) bestTime = time;
        time = new float[] { 0, 0, 0, 0 };
    }

    public void incrementTimer(int inc)
    {
        time[1] += inc;
        if(time[1] >= 60)
        {
            int tmp = (int)(time[1] / 60);
            time[1] = time[1] % 60;
            time[2] += tmp;
            if(time[2] >= 60)
            {
                tmp = (int)(time[2] / 60);
                time[2] = time[2] % 60;
                time[3] += tmp;
            }
        }
    }

    public string timeToString()
    {
        string tmp = string.Format("{0:N3}", time[0]).Substring(2, 2);
        string result = string.Format("{0:D2} : {1:D2} : {2:D2} : {3}", (int)time[3], (int)time[2], (int)time[1], tmp);
        return result;
    }

    public void incrementGeneration(int value)
    {
        generation += value;
        generationText.text = "Generasi " + generation;
        string tmp = string.Format("{0:N3}", bestTime[0]).Substring(2, 2);
        string bestTimeStr = string.Format("{0:D2} : {1:D2} : {2:D2} : {3}", (int)bestTime[3], (int)bestTime[2], (int)bestTime[1], tmp);
        bestTimeText.text = "Best Time:\n" + bestTimeStr;
    }

    public int compareBestTime()
    {
        for(int i = time.Length-1; i >= 0; i--)
        {
            if (time[i] > bestTime[i]) return 1;
            else if (time[i] < bestTime[i]) return -1;
        }
        return 0;
    }

    
    void Start()
    {
        resetTimer();
        incrementGeneration(1);
    }

    // Update is called once per frame
    void Update()
    {
        time[0] += (Time.deltaTime * playSpeed);
        if(time[0] >= 1.0f)
        {
            int tmp = (int)(time[0] / 1);
            time[0] = time[0] % 1.0f;
            incrementTimer(tmp);
        }
        timerText.text = timeToString();
    }

    public static float getSecond(float[] time)
    {
        float result = 0f;
        result += time[0];
        result += time[1];
        result += time[2] * 60;
        result += time[3] * 3600;
        return result;
    }
}
