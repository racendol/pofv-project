﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.UI;

public class BulletNode: IComparable<BulletNode>{
    public double distance;
    public double degree;
    public double velocityX;
    public double velocityY;

    public BulletNode(double distance, double degree, double velocityX, double velocityY){
        this.distance = distance;
        this.degree = degree;
        this.velocityX = velocityX;
        this.velocityY = velocityY;
    }

    public int CompareTo(BulletNode other){
        if (other == null) return 1;
        if (distance < other.distance)
            return -1;
        else if (distance > other.distance)
            return 1;
        else
            return 0;
    }
}

public class BulletObject: IComparable<BulletObject>
{
    public double danger;
    public double distance;
    public double angle;
    public double bulletAngle;
    public double bulletSpeed;

    public BulletObject(double danger, double distance, double angle, double bulletAngle, double bulletSpeed)
    {
        this.danger = danger;
        this.distance = distance;
        this.angle = angle;
        this.bulletAngle = bulletAngle;
        this.bulletSpeed = bulletSpeed;
    }

    public int CompareTo(BulletObject other)
    {
        if (other == null) return 1;
        if (danger > other.danger)
            return -1;
        else if (danger < other.danger)
            return 1;
        else
            return 0;
    }

    public override String ToString()
    {
        return danger.ToString();
    }
}

public class Simulator : MonoBehaviour
{
    private List<NeuralNetwork> agents = new List<NeuralNetwork>();
    private List<GameObject> players = new List<GameObject>();
    private GameObject[] enemies;
    private List<int> nodeList;

    [Header("General Options")]
    public GameObject playerFab;
    public Camera orthographicCamera;
    public RectTransform border;
    public int[] nodeCount = { 36, 4 };
    public bool debugRay = true;
    public bool hitOnly = false;
    private int playSpeed = 1;

    [Header("Player Options")]
    public int totalPlayer = 1;
    public bool playerInput = false;
    public bool borderPenalty = true;
    public bool borderSuddenDeath = false;
    public bool playerRandomInitialSpawn = false;

    [Header("Input Options")]
    public bool standard = true;
    public int maxBullet = 60;
    public bool kirishima = false;
    public int detectRadius = 4;
    public int indices = 36;
    public bool LIDAR = false;
    public bool cirlcleDetection = false;
    public float circleSize = 1;

    [Header("Generation Options")]
    public bool mate = true;
    public bool localSearch = false;
    public bool probabilisticGA = false;
    public bool staticAgent = false;
    //public bool halfRandomize = false;

    [Header("Save Options")]
    public bool saveToggle = false;
    public int saveFrequency = 5;

    [Header("Load Options")]
    public bool loadToggle = false;
    public string loadpath = "";
    [Space(15)]

    private string savepath = "";
    private string logpath = "";

    private TimerScript timer;
    private int timerMinute = 0;

    private int bulletArgsTaken = 4;
    //private int maxBullet;

    // Start is called before the first frame update
    void Awake()
    {
        // Speed up is still broken
        //Application.targetFrameRate = Application.targetFrameRate * playSpeed;
        //Physics2D.velocityIterations = Physics2D.velocityIterations * playSpeed;
        //Physics2D.positionIterations = Physics2D.positionIterations * playSpeed;

        string dateNowString = DateTime.Now.ToString().Replace(" ", "_").Replace("/", "-").Replace(":", "-");
        savepath = Path.Combine(Directory.GetCurrentDirectory(), "save");
        Directory.CreateDirectory(savepath);
        savepath = Path.Combine(savepath, "Result-" + dateNowString);
        Directory.CreateDirectory(savepath);
        Debug.Log("All data saved to " + savepath);

        logpath = Path.Combine(savepath, "LOG-" + dateNowString +  ".csv");
        StreamWriter sw = new StreamWriter(logpath);
        sw.WriteLine("timestamp,generation,time,max_time,max_fitness");
        sw.Close();

        timer = GameObject.FindGameObjectWithTag("Timer").GetComponent<TimerScript>();
        enemies = GameObject.FindGameObjectsWithTag("Enemy");

        if (playerInput)
        {
            totalPlayer = 1;
            nodeList = new List<int>(nodeCount);
            GameObject player = Instantiate(playerFab, transform.position, transform.rotation);
            player.transform.SetParent(border.transform);
            players.Add(player);
            NeuralNetwork agent = new NeuralNetwork(false, nodeList);
            agents.Add(agent);
            player.GetComponent<PlayerBehaviour>().agent = agent;
        }
        else if (!loadToggle)
        {
            nodeList = new List<int>(nodeCount);
            for (int i = 0; i < totalPlayer; i++)
            {
                GameObject player = Instantiate(playerFab, transform.position, transform.rotation);
                player.transform.SetParent(border.transform);
                players.Add(player);
                agents.Add(new NeuralNetwork(true, nodeList));
                players[i].GetComponent<PlayerBehaviour>().agent = agents[i];
            }
        }
        else load(loadpath);
        StartCoroutine(bulletTrackingProto());
    }

    /*
    private void OnDrawGizmos()
    {
        foreach(GameObject player in players)
        {
            for (int i = 0; i < indices; i++)
            {
                float theta = 360 / indices;
                Vector2 direction = new Vector2();
                direction.x = Mathf.Cos(Mathf.Deg2Rad * theta * i);
                direction.y = Mathf.Sin(Mathf.Deg2Rad * theta * i);
                //RaycastCommand rayCast = new RaycastCommand(player.transform.position, direction, detectRadius, 8, 1);
                Gizmos.DrawLine(player.transform.position, (Vector2)player.transform.position + direction * detectRadius);
                //Debug.Log(hitObject.distance.ToString());
            }
        }
    }
    */

    void Update()
    {
        for (int i = 0; i < totalPlayer; i++)
        {
            GameObject player = players[i];
            if (!player.GetComponent<PlayerBehaviour>().isDead)
            {
                agents[i].fitness += Time.deltaTime;
            }
            /*
            bool notNearWall = (distanceToWallX < 0.98 && distanceToWallX > -0.98 && distanceToWallY < 0.98 && distanceToWallY > -0.98);
            if (!player.GetComponent<PlayerBehaviour>().isDead && (notNearWall || !borderPenalty))
            {
                agents[i].fitness += Time.deltaTime;
            }
            else if (!player.GetComponent<PlayerBehaviour>().isDead && !notNearWall)
            {
                if (borderSuddenDeath)
                {
                    player.GetComponent<PlayerBehaviour>().isDead = true;
                    player.SetActive(false);
                }
                //agents[i].fitness -= Time.deltaTime;
            }
            */
        }
        if(timer.time[2] > timerMinute)
        {
            for (int i = 0; i < enemies.Length; i++)
            {
                enemies[i].GetComponent<EnemyBehaviour>().difficultyUp();
                timerMinute++;
            }
        }
    }

    public void inputStandard(NeuralNetwork agent, GameObject[] bullets, GameObject player)
    {
        List<BulletObject> bullist = new List<BulletObject>();

        for (int i = 0; i < bullets.Length; i++) {
            GameObject bullet = bullets[i];
            Rigidbody2D bulletRb2d = bullet.GetComponent<Rigidbody2D>();
            double distance = Vector2.Distance(bullet.transform.position, player.transform.position);
            
            double inputDistance = (distance*2/calculateDiagonal(border.rect.width, border.rect.height)) - 1;

            Vector2 bulletRelativePlayer = bullet.transform.position - player.transform.position;
            double degree = Vector2.SignedAngle(bulletRelativePlayer, Vector2.up);
            if(degree < 0){
                degree = degree + 360;
            }
            degree = (degree / 180.0) - 1;

            double velocityX, velocityY = 0;
            velocityX = bulletRb2d.velocity.x/10;
            if (player.transform.position.x - bullet.transform.position.x > 0){
                velocityX *= -1;
            }

            velocityY = bulletRb2d.velocity.y/10;
            if (player.transform.position.y - bullet.transform.position.y < 0){
                velocityY *= -1;
            }

            Vector2 playerRelativeBullet = player.transform.position - bullet.transform.position;
            Vector2 bulletVelocityRelative = bulletRb2d.velocity;
            double bulletAngle = Vector2.SignedAngle(playerRelativeBullet, bulletVelocityRelative);
            if (bulletAngle < 0)
            {
                bulletAngle = bulletAngle + 360;
            }
            double bulletAngleCos = Math.Cos(Math.PI * bulletAngle / 180);
            double bulletAngleSpeed = bulletAngleCos * bulletRb2d.velocity.magnitude;

            double bulletSpeed = (bulletRb2d.velocity.magnitude) * (0.1/ calculateDiagonal(border.rect.width, border.rect.height));

            double danger = bulletAngleSpeed / distance;

            bullist.Add(new BulletObject(danger, inputDistance, degree, bulletAngleCos, bulletSpeed));
        }

        bullist.Sort();

        for (int i = 0; i < bullist.Count && i < maxBullet; i++){
            agent.layers[0][i*4].set_value(bullist[i].distance);
            agent.layers[0][i*4+1].set_value(bullist[i].angle);
            agent.layers[0][i*4+2].set_value(bullist[i].bulletAngle);
            agent.layers[0][i*4+3].set_value(bullist[i].bulletSpeed);
        }

        for (int i = bullets.Length; i < maxBullet; i++)
        {
            agent.layers[0][i * 4].set_value(0);
            agent.layers[0][i * 4 + 1].set_value(0);
            agent.layers[0][i * 4 + 2].set_value(0);
            agent.layers[0][i * 4 + 3].set_value(0);
        }

        double distanceToWallX = (player.transform.localPosition.x / (border.rect.width/2));
        double distanceToWallY = (player.transform.localPosition.y / (border.rect.height/2));

        agent.layers[0][maxBullet * 4].set_value(distanceToWallX);
        agent.layers[0][maxBullet * 4 + 1].set_value(distanceToWallY);
    }

    public void inputKirishima(NeuralNetwork agent, GameObject[] bullets, GameObject player)
    {
        List<BulletObject> bullist = new List<BulletObject>();
        double[] danger = new double[indices];

        for (int i = 0; i < bullets.Length; i++)
        {
            GameObject bullet = bullets[i];
            Rigidbody2D bulletRb2d = bullet.GetComponent<Rigidbody2D>();
            double distance = Math.Abs(Vector2.Distance(bullet.transform.position, player.transform.position));

            if (distance > detectRadius) continue;

            Vector2 bulletRelativePlayer = bullet.transform.position - player.transform.position;
            double degree = Vector2.SignedAngle(bulletRelativePlayer, Vector2.up);
            if (degree < 0)
            {
                degree = degree + 360;
            }
            int indiceObj = (int)Math.Floor(degree / (360 / indices));

            danger[indiceObj] += Math.Pow(1 - distance / detectRadius, 4);
        }

        double totalDanger = 0;
        for(int i = 0; i < indices; i++)
        {
            totalDanger += danger[i];
        }

        for(int i = 0; i < indices; i++)
        {
            agent.layers[0][i].set_value(danger[i]/totalDanger);
        }

        double distanceToWallX = (player.transform.localPosition.x * 2 / border.rect.width);
        double distanceToWallY = (player.transform.localPosition.y * 2 / border.rect.height);

        agent.layers[0][indices].set_value(distanceToWallX);
        agent.layers[0][indices + 1].set_value(distanceToWallY);
    }

    public void inputLidar(NeuralNetwork agent, GameObject[] bullets, GameObject player)
    {
        List<BulletObject> bullist = new List<BulletObject>();
        double[] sensors = new double[indices];
        float theta = 360f / indices;

        for (int i = 0; i < indices; i++)
        {
            Vector2 direction = new Vector2();
            direction.x = Mathf.Cos(Mathf.Deg2Rad * theta * i);
            direction.y = Mathf.Sin(Mathf.Deg2Rad * theta * i);
            //RaycastCommand rayCast = new RaycastCommand(player.transform.position, direction, detectRadius, 8, 1);

            RaycastHit2D hitObject;
            if (cirlcleDetection) hitObject = Physics2D.CircleCast(player.transform.position, circleSize, direction, detectRadius, (1 << 8) + (1 << 9));
            else hitObject = Physics2D.Raycast(player.transform.position, direction, detectRadius, 1 << 8, -100, 100);

            if (hitObject) sensors[i] = hitObject.distance;
            else sensors[i] = detectRadius;

            //Gizmos.DrawLine(player.transform.position, direction * detectRadius);
            //Debug.Log(hitObject.distance.ToString());
            if (debugRay && hitOnly) Debug.DrawRay(player.transform.position, direction * hitObject.distance, Color.red);
            else if(debugRay) Debug.DrawRay(player.transform.position, direction * (float)sensors[i], Color.red);
            if (i == 0 && debugRay) Debug.DrawRay(player.transform.position, direction * detectRadius, Color.blue);
        }

        for (int i = 0; i < bullets.Length; i++)
        {
            GameObject bullet = bullets[i];
            Rigidbody2D bulletRb2d = bullet.GetComponent<Rigidbody2D>();
            double distance = Math.Abs(Vector2.Distance(bullet.transform.position, player.transform.position));

            if (distance > detectRadius) continue;

            Vector2 bulletRelativePlayer = bullet.transform.position - player.transform.position;
            double angle = Vector2.SignedAngle(bulletRelativePlayer, Vector2.up) + (theta / 2);
            if (angle < 0)
            {
                angle = angle + 360;
            }
            int ind = (int)Math.Floor(angle / theta);

            if (sensors[ind] > distance) sensors[ind] = distance;
        }

        for (int i = 0; i < indices; i++)
        {
            agent.layers[0][i].set_value(1 - sensors[i] / detectRadius);
        }
    }

    public void feedForward(NeuralNetwork agent, GameObject player)
    {
        int lastLayer = agent.layers.Count - 1;
        for (int i = 1; i < lastLayer; i++)
        {
            for (int j = 0; j < agent.layers[i].Count; j++)
            {
                double value = 0;
                for (int k = 0; k < agent.layers[i - 1].Count; k++)
                {
                    value += agent.layers[i - 1][k].value * agent.weights[i - 1][k][j];
                }
                value += agent.biases[i][j];
                agent.layers[i][j].set_tanh(value);
            }
        }

        if (agent.layers[lastLayer].Count == 2)
        {
            // If last layer has 2 neurons
            // Output is (left, right), (up, down)
            for (int j = 0; j < agent.layers[lastLayer].Count; j++)
            {
                double value = 0;
                for (int k = 0; k < agent.layers[lastLayer - 1].Count; k++)
                {
                    value += agent.layers[lastLayer - 1][k].value * agent.weights[lastLayer - 1][k][j];
                }
                value += agent.biases[lastLayer][j];
                agent.layers[lastLayer][j].set_value(value * 0.02);
            }
            player.GetComponent<PlayerBehaviour>().setMovement(agent.layers[lastLayer][0].value, agent.layers[lastLayer][1].value);
        }
        else if (agent.layers[lastLayer].Count == 4)
        {
            // if output layer has 4 neurons
            // The output is as follows = { up, down, left, right }
            // With -1 to 0 means inactive and the rest moves at a rate
            for (int j = 0; j < agent.layers[lastLayer].Count; j++)
            {
                double value = 0;
                for (int k = 0; k < agent.layers[lastLayer - 1].Count; k++)
                {
                    value += agent.layers[lastLayer - 1][k].value * agent.weights[lastLayer - 1][k][j];
                }
                value += agent.biases[lastLayer][j];
                agent.layers[lastLayer][j].set_relu(value, 0.04);
            }

            double up = agent.layers[lastLayer][0].value;
            double down = agent.layers[lastLayer][1].value;
            double left = agent.layers[lastLayer][2].value;
            double right = agent.layers[lastLayer][3].value;
            player.GetComponent<PlayerBehaviour>().setMovement(right - left, down - up);
        }
    }

    private void cullPopulation(){
        int agent4 = agents.Count / 4;
        for (int i=0; i<agent4; i++)
        {
            NeuralNetwork child = agents[i].mate(nodeList, agents[i+1]);
            agents[(agent4*2)+i] = child;
        }
        for(int i = agent4*3; i < agents.Count; i++)
        {
            agents[i] = new NeuralNetwork(true, nodeList);
        }
    }


    private void cullPopulation2()
    {
        // Assumes that agent count is always multiply of 4
        int agent4 = agents.Count / 4;
        for (int i = 0; i < agent4; i++)
        {
            NeuralNetwork child = agents[i].randomize(nodeList);
            agents[i + agent4] = child;
            child = agents[i].randomize(nodeList);
            agents[i + agent4*2] = child;
        }
        for(int i = agent4*3; i < agents.Count; i++)
        {
            agents[i] = new NeuralNetwork(true, nodeList);
        }
    }

    // authentic GA experience
    private void cullPopulation3()
    {
        double totalFitness = 0;
        for (int i = 0; i < agents.Count; i++)
        {
            totalFitness += agents[i].fitness;
        }

        List<Double> chances = new List<Double>();
        for (int i = 0; i < agents.Count; i++)
        {
            chances.Add(agents[i].fitness / totalFitness);
        }

        List<NeuralNetwork> newAgents = new List<NeuralNetwork>();
        for (int i = 0; i < agents.Count; i++)
        {
            NeuralNetwork parent1 = null;
            NeuralNetwork parent2 = null;
            // find parent 1
            double random = UnityEngine.Random.Range(0f, 1f);
            double cumulative = 0;
            for(int j = 0; j < chances.Count; j++)
            {
                cumulative += chances[j];
                if (cumulative >= random) parent1 = agents[j];
            }
            if (parent1 == null) parent1 = agents[0];
            // find parent 2
            random = UnityEngine.Random.Range(0f, 1f);
            cumulative = 0;
            for (int j = 0; j < chances.Count; j++)
            {
                cumulative += chances[j];
                if (cumulative >= random) parent2 = agents[j];
            }
            if (parent2 == null) parent2 = agents[0];
            NeuralNetwork child = parent1.mate(nodeList, parent2);
            newAgents.Add(child);
        }
        agents = newAgents;
    }

    private void cullPopulation4()
    {
        for (int i = agents.Count/2; i < agents.Count; i++)
        {
            agents[i] = new NeuralNetwork(true, nodeList);
        }
    }

    private void cullPopulationButNotReally()
    {
        return;
    }

    private void cullPopulationRandom()
    {
        for (int i = 0; i < agents.Count; i++)
        {
            agents[i] = new NeuralNetwork(true, nodeList);
        }
    }

    private double calculateDiagonal(double vx, double vy){
        return Math.Sqrt(Math.Pow(vx, 2) + Math.Pow(vy, 2));
    }
    
    IEnumerator bulletTrackingProto(){
        while (true){
            bool isAllDead = true;
            GameObject[] bullets = GameObject.FindGameObjectsWithTag("Bullet");
            for(int i = 0; i < totalPlayer; i++) {
                GameObject player = players[i];
                if(!player.GetComponent<PlayerBehaviour>().isDead){
                    isAllDead = false;

                    if (true)
                    {
                        if (standard) inputStandard(agents[i], bullets, player);
                        else if (kirishima) inputKirishima(agents[i], bullets, player);
                        else inputLidar(agents[i], bullets, player);
                        feedForward(agents[i], player);
                    }
                }
            }
            if(isAllDead){
                agents.Sort();

                log_write();
                if(saveToggle && timer.generation % saveFrequency == 0)
                {
                    save();
                }

                if (mate) cullPopulation();
                else if (localSearch) cullPopulation2();
                else if (probabilisticGA) cullPopulation3();
                else if (staticAgent) cullPopulationButNotReally();
                else cullPopulationRandom();

                for (int i=0; i< bullets.Length; i++)
                {
                    Destroy(bullets[i]);
                }

                if(playerRandomInitialSpawn){        
                    Vector2 pos = transform.position;
                    float rand = UnityEngine.Random.Range(0f, 1f);
                    float rand2 = UnityEngine.Random.Range(-10f, 10f);
                    pos.x = rand2;
                    if(rand > 0.5){    
                        pos.y *= -1;                    
                    }
                    transform.position = pos;
                }

                for(int i = 0; i < totalPlayer; i++) {
                    players[i].transform.position = transform.position;
                    agents[i].resetInput();
                    agents[i].fitness = 0;
                    players[i].GetComponent<PlayerBehaviour>().agent = agents[i];
                    players[i].SetActive(true);
                    players[i].GetComponent<PlayerBehaviour>().isDead = false;
                    players[i].GetComponent<PlayerBehaviour>().setMovement(0, 0);
                }
                for(int i = 0; i < enemies.Length; i++)
                {
                    enemies[i].GetComponent<EnemyBehaviour>().resetBullet();
                }
                timerMinute = 0;
                timer.resetTimer();
                timer.incrementGeneration(1);

            }
            if (LIDAR) yield return new WaitForFixedUpdate();
            else yield return new WaitForSeconds(0.1f);
        }
    }

    private void save()
    {
        List<string> fitness = new List<string>();
        List<string> weights = new List<string>();
        List<string> biases = new List<string>();

        string inputType = "";
        string genAlg = "";

        if (standard) inputType = InputType.STANDARD.ToString();
        else if (kirishima) inputType = InputType.KIRISHIMA.ToString();
        else inputType = InputType.LIDAR.ToString();

        if (mate) genAlg = GenAlg.MATE.ToString();
        else if (localSearch) genAlg = GenAlg.LOCAL.ToString();
        else if (probabilisticGA) genAlg = GenAlg.PROBABILISTIC.ToString();
        else if (staticAgent) genAlg = GenAlg.STATIC.ToString();
        else genAlg = GenAlg.RANDOM.ToString();

        foreach (NeuralNetwork agent in agents)
        {
            fitness.Add(string.Format("{0:N5}", agent.fitness));

            // layer loop
            for(int i = 0; i < nodeList.Count; i++)
            {
                for(int j = 0; j < nodeList[i]; j++)
                {
                    biases.Add(string.Format("{0:N5}", agent.biases[i][j]));
                    if(nodeList.Count-i != 1)
                    {
                        for (int k = 0; k < nodeList[i+1]; k++)
                        {
                            weights.Add(string.Format("{0:N5}", agent.weights[i][j][k]));
                        }
                    }
                }
            }
        }

        saveFile saveObj = new saveFile(timer.generation, totalPlayer, inputType, genAlg, nodeList, fitness, weights, biases);
        String json = JsonUtility.ToJson(saveObj);
        string file = Path.Combine(savepath, "Save-" + timer.generation.ToString() + ".json");
        StreamWriter sw = new StreamWriter(file);
        sw.WriteLine(json);
        sw.Close();
        Debug.Log("Succesfully print to " + file);
    }

    private void load(string loadfile)
    {
        StreamReader sr = new StreamReader(loadfile);
        string json = sr.ReadLine();
        saveFile saveObj = JsonUtility.FromJson<saveFile>(json);
        sr.Close();

        timer.generation = saveObj.generation;
        totalPlayer = saveObj.agentSize;
        nodeList = saveObj.nodes;

        standard = false;
        kirishima = false;
        LIDAR = false;

        if (saveObj.input == InputType.LIDAR.ToString()) LIDAR = true;
        else if (saveObj.input == InputType.KIRISHIMA.ToString()) kirishima = true;
        else standard = true;

        mate = false;
        localSearch = false;
        probabilisticGA = false;
        staticAgent = false;

        if (saveObj.GA == GenAlg.MATE.ToString()) mate = true;
        else if (saveObj.GA == GenAlg.LOCAL.ToString()) localSearch = true;
        else if (saveObj.GA == GenAlg.PROBABILISTIC.ToString()) probabilisticGA = true;
        else if (saveObj.GA == GenAlg.STATIC.ToString()) staticAgent = true;

        for (int i = 0; i < totalPlayer; i++)
        {
            GameObject player = Instantiate(playerFab, transform.position, transform.rotation);
            player.transform.SetParent(border.transform);
            players.Add(player);
            agents.Add(new NeuralNetwork(true, nodeList));
            players[i].GetComponent<PlayerBehaviour>().agent = agents[i];
        }

        // see first agent of loaded file
        string file = Path.Combine(savepath, "Load-agent-weight.csv");
        StreamWriter sw = new StreamWriter(file);

        string file2 = Path.Combine(savepath, "Load-agent-bias.csv");
        StreamWriter sw2 = new StreamWriter(file2);

        int biasIndex = 0;
        int weightIndex = 0;
        for(int a = 0; a < totalPlayer; a++)
        {
            agents[a].fitness = double.Parse(saveObj.fitness[a]);

            // layer loop
            for (int i = 0; i < nodeList.Count; i++)
            {
                // bias loop
                for (int j = 0; j < nodeList[i]; j++)
                {
                    agents[a].biases[i][j] = double.Parse(saveObj.biases[biasIndex++]);
                    if(a == 0)
                    {
                        sw2.WriteLine(agents[a].biases[i][j].ToString() + ",");
                    }
                    if (nodeList.Count - i != 1)
                    {
                        // weight loop
                        for (int k = 0; k < nodeList[i + 1]; k++)
                        {
                            agents[a].weights[i][j][k] = double.Parse(saveObj.weights[weightIndex++]);
                            if(a == 0)
                            {
                                sw.WriteLine(agents[a].weights[i][j][k].ToString() + ",");
                            }
                        }
                    }
                }
            }
        }
        sw.Close();
        sw2.Close();
    }

    private void log_write()
    {
        //"timestamp,generation,time,max_time,max_fitness";
        float time = TimerScript.getSecond(timer.time);
        float maxTime = TimerScript.getSecond(timer.bestTime);
        if(time > maxTime) maxTime = time;
        double maxFitness = agents[0].fitness;

        StreamWriter sw = new StreamWriter(logpath, true);
        string logString = string.Format("{0},{1:D},{2:N5},{3:N5},{4:N5}", DateTime.Now.ToString(), timer.generation, time, maxTime, maxFitness);
        sw.WriteLine(logString);
        sw.Close();
    }
}

[Serializable()]
public class saveFile
{
    public int generation;
    public int agentSize;
    public string input;
    public string GA;
    public List<int> nodes;
    public List<string> fitness;
    public List<string> weights;
    public List<string> biases;

    public saveFile(int generation, int agentSize, string input, string GA, List<int> nodes, List<string> fitness, List<string> weights, List<string> biases)
    {
        this.generation = generation;
        this.agentSize = agentSize;
        this.input = input;
        this.GA = GA;
        this.nodes = nodes;
        this.fitness = fitness;
        this.weights = weights;
        this.biases = biases;
    }
}

public enum GenAlg
{
    MATE,
    LOCAL,
    PROBABILISTIC,
    STATIC,
    RANDOM
}

public enum InputType
{
    STANDARD,
    KIRISHIMA,
    LIDAR
}

/*
Here lies timothy's hopes and dreams


    //* how the save function works is basically for each agent they will have a folder
    //* the folder will contain 2 files, consisting of their biases and weights
    //* 

private void save()
{
    string folderName = Directory.GetCurrentDirectory();
    string pathString = Path.Combine(folderName, "save");
    Directory.CreateDirectory(pathString);
    foreach (NeuralNetwork agent in agents)
    {
        pathString = System.IO.Path.Combine(pathString, agent.name.ToString());
        System.IO.Directory.CreateDirectory(pathString);
        string fileName = "bias.txt";
        pathString = System.IO.Path.Combine(pathString, fileName);
        StreamWriter sw = new StreamWriter(pathString);
        for (int i = 0; i < agent.biases.Count; i++)
        {
            for (int j = 0; j < agent.biases[i].Count; j++)
            {
                sw.WriteLine(agent.biases[i][j]);
            }
        }
        sw.Close();
        pathString = System.IO.Path.Combine(folderName, "save");
        pathString = System.IO.Path.Combine(pathString, agent.name.ToString());
        fileName = "weight.txt";
        pathString = System.IO.Path.Combine(pathString, fileName);
        sw = new StreamWriter(pathString);
        for (int i = 0; i < agent.weights.Count; i++)
        {
            for (int j = 0; j < agent.weights[i].Count; j++)
            {
                for (int k = 0; k < agent.weights[i][j].Count; k++)
                {
                    sw.WriteLine(agent.weights[i][j][k]);
                }
            }
        }
        sw.Close();
        pathString = System.IO.Path.Combine(folderName, "save");
    }
}


// reads all saved agents

private void load()
{
    string folderName = System.IO.Path.Combine(Directory.GetCurrentDirectory(), "save");
    string line;
    // string pathName = System.IO.Path.Combine(folderName, agentNumber.ToString());
    // string biasFile = System.IO.Path.Combine(pathName, "bias.txt");
    // string weightFile = System.IO.Path.Combine(pathName, "weight.txt");
    // StreamReader sr = new StreamReader(biasFile);

    for (int n = 0; n < agents.Count; n++)
    {
        string pathName = System.IO.Path.Combine(folderName, n.ToString());
        string biasFile = System.IO.Path.Combine(pathName, "bias.txt");
        string weightFile = System.IO.Path.Combine(pathName, "weight.txt");
        StreamReader sr = new StreamReader(biasFile);
        // bias [0]
        for (int i = 0; i < 242; i++)
        {
            line = sr.ReadLine();
            // test.biases[0][i] = float.Parse(line);
        }
        // bias [1]
        for (int i = 0; i < 60; i++)
        {
            line = sr.ReadLine();
            // test.biases[1][i] = float.Parse(line);
        }
        // bias [2]
        for (int i = 0; i < 60; i++)
        {
            line = sr.ReadLine();
            // test.biases[2][i] = float.Parse(line);
        }
        // bias [3]
        for (int i = 0; i < 2; i++)
        {
            line = sr.ReadLine();
            // test.biases[3][i] = float.Parse(line);
        }
        sr.Close();
        sr = new StreamReader(weightFile);
        // weight [0]
        for (int i = 0; i < 242; i++)
        {
            for (int j = 0; j < 60; j++)
            {
                line = sr.ReadLine();
                // test.weights[0][i][j] = float.Parse(line);
            }
        }
        // weight [1]
        for (int i = 0; i < 60; i++)
        {
            for (int j = 0; j < 60; j++)
            {
                line = sr.ReadLine();
                // test.weights[1][i][j] = float.Parse(line);
            }
        }
        // weight [2]
        for (int i = 0; i < 60; i++)
        {
            for (int j = 0; j < 2; j++)
            {
                line = sr.ReadLine();
                // test.weights[2][i][j] = float.Parse(line);
            }
        }
        sr.Close();
    }
    Debug.Log("load");
}
*/