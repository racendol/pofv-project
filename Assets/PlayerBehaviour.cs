﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerBehaviour : MonoBehaviour
{
    [Header("Player Values")]
    public float speed = 10;
    // Border penalty is still broken on the CollisionStay
    public bool borderPenalty = false;

    [HideInInspector]
    public bool isDead = false;

    [HideInInspector]
    public double fitness;

    [HideInInspector]
    public NeuralNetwork agent;

    [HideInInspector]
    public RectTransform border;

    [HideInInspector]
    public int playSpeed = 1;

    private bool playerInput;

    // x, y
    private float[] playerSize = { 0, 0 };

    private Rigidbody2D rb2d;
    private Vector2 move;
    private Vector2 initPosition;

    // left, right, up, down
    private float[] borderSize = { 0, 0, 0, 0 };


    public void setMovement(double vertical, double horizontal)
    {
        move = new Vector2((float)vertical, (float)horizontal);
        //lerpTime = 0;
    }

    // Start is called before the first frame update
    void Start()
    {
        rb2d = GetComponent<Rigidbody2D>();
        initPosition = transform.position;
        playerSize[0] = GetComponent<SpriteRenderer>().bounds.size.x;
        playerSize[1] = GetComponent<SpriteRenderer>().bounds.size.y;

        playerInput = GameObject.FindGameObjectWithTag("GameController").GetComponent<Simulator>().playerInput;

        border = GameObject.FindGameObjectWithTag("GameController").GetComponent<Simulator>().border;

        borderSize[0] = -(border.rect.width / 2) + border.transform.position.x + (playerSize[0] / 2);
        borderSize[1] = (border.rect.width / 2) + border.transform.position.x - (playerSize[0] / 2);
        borderSize[2] = (border.rect.height / 2) + border.transform.position.y - (playerSize[1] / 2);
        borderSize[3] = -(border.rect.height / 2) + border.transform.position.y + (playerSize[1] / 2);
        //Debug.LogFormat("{0};{1};{2};{3}", borderSize[0], borderSize[1], borderSize[2], borderSize[3]);
    }

    void Update()
    {
        fitness = agent.fitness;
        clampPlayerMovement();
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        float currSpeed = speed*playSpeed;
        if (playerInput)
        {
            move = new Vector2(Input.GetAxisRaw("Horizontal"), Input.GetAxisRaw("Vertical"));
            if (Input.GetAxisRaw("Focus") > 0){
                currSpeed *= 0.5f;
            }
        }
        rb2d.velocity = move*currSpeed;
    }

    void clampPlayerMovement()
    {
        Vector3 position = transform.position;

        position.x = Mathf.Clamp(position.x, borderSize[0], borderSize[1]);
        position.y = Mathf.Clamp(position.y, borderSize[3], borderSize[2]);

        transform.position = position;
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.tag == "Bullet"){
            this.isDead = true;
            gameObject.SetActive(false);
            agent.fitness -= 10;
            //agent.fitness = agent.fitness / Simulator.agentAliveCount;
            //Simulator.agentAliveCount--;
            //Debug.Log(Simulator.agentAliveCount);
        }

        if (other.gameObject.tag == "Player"){
            Physics2D.IgnoreCollision(other.gameObject.GetComponent<Collider2D>(), GetComponent<Collider2D>());
        }

        if (other.gameObject.tag == "Border")
        {
            Physics2D.IgnoreCollision(other.gameObject.GetComponent<Collider2D>(), GetComponent<Collider2D>());
            if (borderPenalty) agent.fitness -= 10;
        }
        return;
    }

    private double calculateVelocity(float vx, float vy){
        return Math.Sqrt(Math.Pow(vx, 2) + Math.Pow(vy, 2));
    }

    private void OnBecameInvisible() {
        transform.position = initPosition;
    }
}