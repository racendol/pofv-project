﻿using System;
using System.Collections;
using System.Collections.Generic;

public class Matrix
{
    public int x;
    public int y;
    public double[,] mat;

    public Matrix(int x, int y)
    {
        this.x = x;
        this.y = y;
        this.mat = new double[y, x];
    }

    public Matrix(double[,] mat)
    {
        this.x = mat.Rank;
        this.y = mat.Length / mat.Rank;
        this.mat = mat;
    }

    public static Matrix multiplyMatrix(Matrix obj1, Matrix obj2)
    {
        Matrix result = new Matrix(obj2.x, obj1.y);
        for (int i = 0; i < obj1.y; i++)
        {
            for (int j = 0; j < obj2.x; j++)
            {
                double tmp = 0;
                for (int k = 0; k < obj2.y; k++)
                {
                    tmp += obj1.get(k, i) * obj2.get(j, k);
                }
                result.insert(j, i, tmp);
            }
        }
        return result;
    }

    public static Matrix addMatrix(Matrix obj1, Matrix obj2)
    {
        Matrix result = new Matrix(obj2.x, obj1.y);
        for (int i = 0; i < obj1.y; i++)
        {
            for (int j = 0; j < obj1.x; j++)
            {
                result.insert(j, i, obj1.get(j, i) + obj2.get(j, i));
            }
        }
        return result;
    }

    public static double Sigmoid(double value)
    {
        double k = Exp(value);
        return k / (1.0 + k);
    }

    public static double Exp(double val)
    {
        long tmp = (long)(1512775 * val + 1072632447);
        return BitConverter.Int64BitsToDouble(tmp << 32);
    }

    public void insert(int x, int y, double value)
    {
        mat[y, x] = value;
    }

    public double get(int x, int y)
    {
        return mat[y, x];
    }

    public string toString()
    {
        return mat.ToString();
    }
}
