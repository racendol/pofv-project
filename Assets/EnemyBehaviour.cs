﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyBehaviour : MonoBehaviour
{
    [Header("General Options")]
    public GameObject bulletFab;

    [Header("Bullet Values")]
    public float DefaultDelay = 0.2f;
    public float DefaultBulletSpeed = 10;
    public float startRotation = 0;
    public float rotationSpeed = 7;

    [Header("Bullet Options")]
    public bool tracking = true;
    public bool splitShot = false;
    public bool changingShot = false;

    [HideInInspector]
    public RectTransform border;

    [HideInInspector]
    public int playSpeed = 1;

    private float delay;
    private float timePassed;

    private float bulletSpeed;

    private double currentAngle = 0;
    private Vector2 currentVel = new Vector2(0, 0);

    private float[] borderSize = { 0, 0, 0, 0 };

    private GameObject[] players;

    private float counter;


    void Start()
    {
        currentAngle = startRotation;
        delay = DefaultDelay;
        bulletSpeed = DefaultBulletSpeed;

        border = GameObject.FindGameObjectWithTag("GameController").GetComponent<Simulator>().border;

        borderSize[0] = -(border.rect.width / 2) + border.transform.position.x;
        borderSize[1] = (border.rect.width / 2) + border.transform.position.x;
        borderSize[2] = (border.rect.height / 2) + border.transform.position.y;
        borderSize[3] = -(border.rect.height / 2) + border.transform.position.y;
    }

    // Update is called once per frame
    void Update()
    {
        timePassed += Time.deltaTime;
        
        if (timePassed >= delay/playSpeed){
            if (tracking)
            {
                shootBulletTracking();
            }
            else
            {
                shootBulletRotating();
            }
            timePassed = 0;
        }

        if(changingShot){
            counter += Time.deltaTime;
            if (counter > 10){
                tracking = false;
            }
            
            if (counter > 15){
                tracking = true;
                counter = 0;
            }
        }
    }

    public void resetBullet()
    {
        timePassed = 0;
        counter = 0;
        currentAngle = startRotation;
        currentVel = new Vector2(0, 0);
        delay = DefaultDelay;
        bulletSpeed = DefaultBulletSpeed;

        Vector2 pos = transform.position;
        transform.position = pos;
    }

    public void difficultyUp(){
        delay /= 1.2f;
        bulletSpeed *= 1.2f;
    }

    public void difficultyDown(){
        delay *= 1.2f;
        bulletSpeed /= 1.2f;
    }

    void shootBulletTracking()
    {
        players = GameObject.FindGameObjectsWithTag("Player");
        foreach (GameObject player in players)
        {
            GameObject bullet = Instantiate(bulletFab, transform.position, transform.rotation);
            bullet.GetComponent<BulletBehaviour>().borderSize = borderSize;
            //homing
            Rigidbody2D bulletRb2d = bullet.GetComponent<Rigidbody2D>();
            Vector2 velocity = new Vector2();
            if (player != null)
            {
                velocity = ((Vector2)(player.transform.position - bullet.transform.position)).normalized * bulletSpeed * playSpeed;
                if(players.Length <= 10){
                    velocity.x += UnityEngine.Random.Range(-1f, 1f);
                    velocity.y += UnityEngine.Random.Range(-1f, 1f);
                    velocity.x = Mathf.Min(velocity.x, 10);
                    velocity.y = Mathf.Min(velocity.y, 10);
                }

            }
            else
            {
                velocity = new Vector2(0, -bulletSpeed) * playSpeed;
            }
            bulletRb2d.velocity = velocity;
        }
    }

    void shootBulletRotating()
    {
        for (int i = 0; i < 6; i++){
            GameObject bullet = Instantiate(bulletFab, transform.position, transform.rotation);
            bullet.GetComponent<BulletBehaviour>().borderSize = borderSize;
            Rigidbody2D bulletRb2d = bullet.GetComponent<Rigidbody2D>();
            currentVel.x = (float)(bulletSpeed * Mathf.Cos((float)(Mathf.Deg2Rad * (currentAngle + 60*i))));
            currentVel.y = (float)(bulletSpeed * Mathf.Sin((float)(Mathf.Deg2Rad * (currentAngle + 60*i))));
            currentVel = currentVel * playSpeed;
            bulletRb2d.velocity = currentVel;
        }
        currentAngle += rotationSpeed;
    }
}
