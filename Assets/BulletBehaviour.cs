﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletBehaviour : MonoBehaviour
{
    [HideInInspector]
    public float[] borderSize = { 0, 0, 0, 0 };

    private void FixedUpdate()
    {
        if(transform.position.x < borderSize[0] || transform.position.x > borderSize[1] || transform.position.y > borderSize[2] || transform.position.y < borderSize[3])
        {
            Destroy(gameObject);
        }
    }

    private void OnBecameInvisible() {
        Destroy(gameObject);
    }
}
